#include "pch.h"
#include <iostream>
#include <string>
#include "Help.h"
#include <Windows.h>

using namespace std;

States Help::runState() {
	displayHelp();
	return getNextState();
}

void Help::displayHelp() {
	cout << "Zorkish::Help" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << "The following commands are supported :" << endl;
	cout << "quit," << endl;
	cout << "hiscore (for testing)" << endl;
	
}

States Help::getNextState() {
	string opt;
	cin >> opt;

	if (opt == "quit") return StateQuit;
	else if (opt == "hiscore") return StateNewHighScore;
	else return StateHelp;


}
