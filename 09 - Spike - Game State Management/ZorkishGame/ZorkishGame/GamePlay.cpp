#include "GamePlay.h"
#include "pch.h"
#include <iostream>
#include <string>
#include <Windows.h>

using namespace std;

States GamePlay::runState() {
	
	runGame();
	return getNextState();

}

void GamePlay::runGame() {
	
	cout << "Welcome to Zorkish : Void World";
	cout << "This world is simple and pointless. Used it to test Zorkish Phase 1 spec.";
	cout << endl;
	cout << ":>";

	
}

States GamePlay::getNextState() {
	
	string opt;
	cin >> opt;

	if (opt == "quit") {
		cout << "Your adventure has ended without fame or fortune.";
		while (GetAsyncKeyState(VK_RETURN)) {}

		while (!GetAsyncKeyState(VK_RETURN)) {}
		return StateQuit;
	}
	else if (opt == "hiscore"){
		cout << "You have entered the magic word and will now see the 'New High Score' screen.";
		while (GetAsyncKeyState(VK_RETURN)) {}

		while (!GetAsyncKeyState(VK_RETURN)) {}
		return StateNewHighScore;
		
	}
	else return StateGameplay;

}