#pragma once
#include "State.h"
class GamePlay : public State
{
public:
	virtual States runState();
	void runGame();
	States getNextState();
};

