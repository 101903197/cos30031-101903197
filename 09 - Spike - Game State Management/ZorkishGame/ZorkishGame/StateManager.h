#pragma once
#include "State.h"

class StateManager
{
public:
	StateManager();

	void setState(States aState);
	void run();
	States getState();

private:
	State* fCurrentState;
	bool fQuitGame;
	States fCurrentGameState;

};
