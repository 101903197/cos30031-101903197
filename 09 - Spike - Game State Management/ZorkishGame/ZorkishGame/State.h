#pragma once

enum States
{
	StateMainMenu,
	StateAbout,
	StateHelp,
	StateSelectAdventure,
	StateGameplay,
	StateNewHighScore,
	StateViewHallOfFame,
	StateQuit,
	

};

class State
{

public:
	virtual States runState() = 0;
};

