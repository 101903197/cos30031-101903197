#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <SDL_ttf.h>
#undef main

using namespace std;

int main() {

	bool quit = false;
	SDL_Event event;
	int x = 288;
	int y = 208;

	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Window* window = SDL_CreateWindow("Collision Detection", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, 0);
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);

	SDL_Rect rect1 = { x, y, 100, 100 };
	SDL_Rect rect2 = { 100, 100, 100, 80 };
	
	TTF_Init();

	TTF_Font * font = TTF_OpenFont("arial.ttf", 18);
	SDL_Color Black = { 0, 0, 0 };
	
	

	if (TTF_Init()<0) {
		cout << "TTF is not initialised." << endl;
	}
	
	if (font==NULL) {
		cout << "Could not load text !!" << endl;
	}
	
	SDL_Surface* surfaceMessage = TTF_RenderText_Solid(font, "No Collision Detected", Black);
	SDL_Texture* Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);

	int texW = 0;
	int texH = 0;
	SDL_QueryTexture(Message, NULL, NULL, &texW, &texH);

	SDL_Rect Message_rect = { 0, 0, texW, texH };
	

	// handle events

	while (!quit)
	{
		SDL_Delay(10);
		SDL_PollEvent(&event);

		switch (event.type)
		{
		case SDL_QUIT:
			quit = true;
			break;
			
			
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym)
			{
			case SDLK_LEFT:  rect1.x--; break;
			case SDLK_RIGHT: rect1.x++; break;
			case SDLK_UP:    rect1.y--; break;
			case SDLK_DOWN:  rect1.y++; break;
			}
			break;
			
		}

		SDL_bool collision = SDL_HasIntersection(&rect1, &rect2);

		SDL_SetRenderDrawColor(renderer, 242, 242, 242, 255);
		SDL_RenderClear(renderer);

		
		SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
		SDL_RenderFillRect(renderer, &rect1);


		SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
		SDL_RenderFillRect(renderer, &rect2);

		if (collision) {
			
			surfaceMessage = TTF_RenderText_Solid(font, "Collision Detected", Black);
			Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);

			
			SDL_RenderCopy(renderer, Message, NULL, &Message_rect);
			
		}
		else {
		
			surfaceMessage = TTF_RenderText_Solid(font, "No Collision Detected", Black);
			Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);


			SDL_RenderCopy(renderer, Message, NULL, &Message_rect);
			
		}
		
		SDL_RenderPresent(renderer);


	}

	// cleanup SDL

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}