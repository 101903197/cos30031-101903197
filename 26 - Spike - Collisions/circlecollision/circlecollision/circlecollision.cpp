#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <iostream>
#undef main

using namespace std;

static const int SCREEN_WIDTH = 1000;
static const int SCREEN_HEIGHT = 1000;

struct Circle
{
    int x, y;
    int r;
};

SDL_Renderer* renderer = nullptr;
SDL_Window* window = nullptr;

class LTexture
{
public:
	//Initializes variables
	LTexture();

	//Deallocates memory
	~LTexture();

	void render(int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);

	int getWidth();
	int getHeight();

private:
	SDL_Texture* mTexture;

	int mWidth;
	int mHeight;

};


bool checkCollision(Circle& a, Circle& b);


//Calculates distance squared between two points
double distanceSquared(int x1, int y1, int x2, int y2);

LTexture gDotTexture;

class Dot
{
public:
	//The dimensions of the dot
	static const int DOT_WIDTH = 100;
	static const int DOT_HEIGHT = 100;

	//Maximum axis velocity of the dot
	static const int DOT_VEL = 5;

	//Initializes the variables
	Dot(int x, int y);

	//Takes key presses and adjusts the dot's velocity
	void handleEvent(SDL_Event& e);

	//Moves the dot and checks collision
	void move(Circle& circle, Circle& circle1);

	//Shows the dot on the screen
	void render();

	//Gets collision circle
	Circle& getCollider();

private:
	//The X and Y offsets of the dot
	int mPosX, mPosY;

	//The velocity of the dot
	int mVelX, mVelY;

	//Dot's collision circle
	Circle mCollider;

	//Moves the collision circle relative to the dot's offset
	void shiftColliders();
};

void LTexture::render(int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { x, y, mWidth, mHeight };

	//Set clip rendering dimensions
	if (clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	//Render to screen

	SDL_RenderCopyEx(renderer, mTexture, clip, &renderQuad, angle, center, flip);
}

LTexture::LTexture()
{
	//Initialize
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;


	SDL_Texture* newTexture = NULL;
	SDL_Surface* circle1 = IMG_Load("dot.bmp");
	mTexture = SDL_CreateTextureFromSurface(renderer, circle1);

	mWidth = circle1->w;
	mHeight = circle1->h;


}

LTexture::~LTexture()
{
}



int LTexture::getWidth()
{
	return mWidth;
}

int LTexture::getHeight()
{
	return mHeight;
}





Dot::Dot(int x, int y)
{
	//Initialize the offsets
	mPosX = 0;
	mPosY = 0;

	//Set collision box dimension
	mCollider.x = DOT_WIDTH;
	mCollider.y = DOT_HEIGHT;

	//Initialize the velocity
	mVelX = 0;
	mVelY = 0;
}

void Dot::move(Circle& circle, Circle& circle1)
{
	//Move the dot left or right
	mPosX += mVelX;
	shiftColliders();

	//If the dot collided or went too far to the left or right
	if ((mPosX - mCollider.r < 0) || (mPosX + mCollider.r > SCREEN_WIDTH) || checkCollision(mCollider, circle) || checkCollision(mCollider, circle1))
	{
		//Move back
		mPosX -= mVelX;
		shiftColliders();
	}

	//Move the dot up or down
	mPosY += mVelY;
	shiftColliders();

	//If the dot collided or went too far up or down
	if ((mPosY - mCollider.r < 0) || (mPosY + mCollider.r > SCREEN_HEIGHT) || checkCollision(mCollider, circle1) || checkCollision(mCollider, circle))
	{
		//Move back
		mPosY -= mVelY;
		shiftColliders();
	}
}

double distanceSquared(int x1, int y1, int x2, int y2)
{
	int deltaX = x2 - x1;
	int deltaY = y2 - y1;
	return deltaX * deltaX + deltaY * deltaY;
}

void Dot::handleEvent(SDL_Event& e)
{
	//If a key was pressed
	if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		//Adjust the velocity
		switch (e.key.keysym.sym)
		{
		case SDLK_UP: mVelY -= DOT_VEL; break;
		case SDLK_DOWN: mVelY += DOT_VEL; break;
		case SDLK_LEFT: mVelX -= DOT_VEL; break;
		case SDLK_RIGHT: mVelX += DOT_VEL; break;
		}
	}
	//If a key was released
	else if (e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		//Adjust the velocity
		switch (e.key.keysym.sym)
		{
		case SDLK_UP: mVelY += DOT_VEL; break;
		case SDLK_DOWN: mVelY -= DOT_VEL; break;
		case SDLK_LEFT: mVelX += DOT_VEL; break;
		case SDLK_RIGHT: mVelX -= DOT_VEL; break;
		}
	}
}

void Dot::render()
{
	//Show the dot
	gDotTexture.render(mPosX - mCollider.r, mPosY - mCollider.r);
}


bool checkCollision(Circle& a, Circle& b)
{
	//Calculate total radius squared
	int totalRadiusSquared = a.r + b.r;
	totalRadiusSquared = totalRadiusSquared * totalRadiusSquared;

	//If the distance between the centers of the circles is less than the sum of their radii
	if (distanceSquared(a.x, a.y, b.x, b.y) < (totalRadiusSquared))
	{
		//The circles have collided
		return true;
	}

	//If not
	return false;
}


int main() {

	bool quit = false;
	SDL_Event event;
	int x = 288;
	int y = 208;
	IMG_Init(IMG_INIT_JPG);
	SDL_Init(SDL_INIT_EVERYTHING);
	window = SDL_CreateWindow("Collision Detection", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
	renderer = SDL_CreateRenderer(window, -1, 0);

	Dot dot(Dot::DOT_WIDTH / 2, Dot::DOT_HEIGHT / 2);
	Dot otherDot(SCREEN_WIDTH / 4, SCREEN_HEIGHT / 4);
	



	// handle events

	while (!quit)
	{
		//Handle events on queue
		while (SDL_PollEvent(&event) != 0)
		{
			//User requests quit
			if (event.type == SDL_QUIT)
			{
				quit = true;
			}

			//Handle input for the dot
			dot.handleEvent(event);
		}

		//Move the dot and check collision
		dot.move(dot.getCollider(), otherDot.getCollider());

		//Clear screen
		SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(renderer);


		//Render dots
		dot.render();
		otherDot.render();

		//Update screen
		SDL_RenderPresent(renderer);
	}

	// cleanup SDL

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}