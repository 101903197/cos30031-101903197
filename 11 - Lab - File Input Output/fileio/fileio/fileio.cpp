// fileio.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>



using namespace std;

struct variable {
	char var1;
	int var2;
	float var3;
};

void printStruct(variable v);
void writeintofile(variable v);
void readfromfile();


int main()
{

	variable v1;
	v1.var1 = 'A';
	v1.var2 = 205;
	v1.var3 = 3.14f;

	writeintofile(v1);

	//readfromfile();
	

}

void printStruct(variable v) {


	cout << " Variable 1 (Char): " + string(1, v.var1) << endl;
	cout << " Variable 2 (Int): " + to_string(v.var2) << endl;
	cout << " Variable 3 (Float): " + to_string(v.var3) << endl;


}

void writeintofile(variable v) {
	try {
		ofstream myFile;
		myFile.open("test1.bin", ios::in | ios::binary);

		myFile.seekp(0);

		myFile.write((char *)&v, sizeof(variable));
		

		myFile.close();
	}
	catch (exception e) {}

	printStruct(v);
}

void readfromfile() {

	try {
		variable v;

		ifstream myFile;
		myFile.open("test1.bin", ios::in);
				

		myFile.read((char *)&v, sizeof(variable));


		printStruct(v);
		myFile.close();
		
	}
	catch (exception e) {}

}

