#pragma once
#include "State.h"
class NewHighScore : public State
{
public:
	virtual States runState();
	void displayNewHighScore();
	States getNextState();

};

