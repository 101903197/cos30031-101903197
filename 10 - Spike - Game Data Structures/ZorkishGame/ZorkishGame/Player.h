#pragma once
#include<string>


using namespace std;

class Player
{
public:
	Player();
	void addItem(string item);
	void openBag();
	void dropItem(string item);

private:
	string bagpack[20];
};

