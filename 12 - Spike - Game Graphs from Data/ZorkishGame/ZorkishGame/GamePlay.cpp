#include "GamePlay.h"
#include "pch.h"
#include <iostream>
#include <string>
#include <Windows.h>
#include "Player.h"
#include <locale>

using namespace std;

GamePlay::GamePlay() {
	
	player = Player();
	flag = true;
}

States GamePlay::runState() {
	
	if (flag) {
		runGame();
		flag = false;
		
	}
	
	int lPlayerLoc = player.getLocation();
	gameObj.displayLocation(lPlayerLoc);
	gameObj.displayMap();
		
	return getNextState();

}

void GamePlay::runGame() {
	
	cout << "Welcome to Zorkish : Void World";
	cout << "This world is simple and pointless. Used it to test Zorkish Phase 1 spec.";
	cout << endl;
	cout << ":>";
	
}

States GamePlay::getNextState() {
	
	locale loc;
	string opt;
	cin >> opt;
	//string opt = tolower(optin,loc);

	if (opt == "quit") {
		cout << "Your adventure has ended without fame or fortune.";
		while (GetAsyncKeyState(VK_RETURN)) {}

		while (!GetAsyncKeyState(VK_RETURN)) {}
		return StateQuit;
	}
	else if (opt == "hiscore"){
		cout << "You have entered the magic word and will now see the 'New High Score' screen.";
		while (GetAsyncKeyState(VK_RETURN)) {}

		while (!GetAsyncKeyState(VK_RETURN)) {}
		return StateNewHighScore;
		
	}
	else if (opt.find("go") != string::npos) {

		string move="";
		cin >> move;

		bool f = gameObj.isMoveValid(player.getLocation(),move);

		player.setLocation(f);

		cout << player.getLocation();
		
		runState();
	
	}
	else return StateGameplay;

}