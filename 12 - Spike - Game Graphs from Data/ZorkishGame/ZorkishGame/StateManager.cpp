#include "StateManager.h"
#include "MainMenu.h"
#include "About.h"
#include "Help.h"
#include "SelectAdventure.h"
#include "HallOfFame.h"
#include "NewHighScore.h"
#include "GamePlay.h"
#include "Quit.h"
#include <stdlib.h>

StateManager::StateManager() {
	fQuitGame = false;
	fCurrentState = 0;
	fCurrentGameState = StateMainMenu;
}

void StateManager::setState(States aState){

	if (!fCurrentState) delete fCurrentState;

	if (aState == StateMainMenu) fCurrentState = new MainMenu();
	else if (aState == StateAbout) fCurrentState = new About();
	else if (aState == StateHelp) fCurrentState = new Help();
	else if (aState == StateSelectAdventure) fCurrentState = new SelectAdventure();
	else if (aState == StateViewHallOfFame) fCurrentState = new HallOfFame();
	else if (aState == StateGameplay) fCurrentState = new GamePlay();
	else if (aState == StateNewHighScore) fCurrentState = new NewHighScore();
	else if (aState == StateQuit) fCurrentState = new Quit;
	fCurrentGameState = aState;
}

void StateManager::run() {
	while (!fQuitGame)
	{
		system("cls");

		setState(fCurrentState->runState());
	}
}

States StateManager::getState()
{
	return fCurrentGameState;
}