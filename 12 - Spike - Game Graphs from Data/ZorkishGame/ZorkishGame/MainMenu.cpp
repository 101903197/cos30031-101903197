#include "MainMenu.h"
#include <iostream>
#include <stdio.h>

using namespace std;

States MainMenu::runState() {

	displayMenu();
	return getNextState();
}

void MainMenu::displayMenu() {
	cout << "Zorkish :: Main Menu" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << "Welcome to Zorkish Adventures" << endl;
	cout << endl;
	cout << "	1. Select Adventure and Play" << endl;
	cout << "	2. Hall Of Fame" << endl;
	cout << "	3. Help" << endl;
	cout << "	4. About" << endl;
	cout << "	5. Quit" << endl;
	cout << endl;
	cout << "Select 1 - 5:> ";
}

States MainMenu::getNextState() {
	int opt;
	cin >> opt;

	if (opt == 1) return StateSelectAdventure;
	else if (opt == 2) return StateViewHallOfFame;
	else if (opt == 3) return StateHelp;
	else if (opt == 4) return StateAbout;
	else if (opt == 5) return StateQuit;
	else return StateMainMenu;
}