#include "pch.h"
#include <iostream>
#include "About.h"
#include <Windows.h>

using namespace std;

States About::runState() {
	displayAbout();
	return getNextState();
}

void About::displayAbout(){
	cout << "Zorkish::About" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << "Written by : Saikiran Solanki (101903197)" << endl;
	cout << "Press Enter to return to the Main Menu" << endl;
}

States About::getNextState() {

	while(GetAsyncKeyState(VK_RETURN)){}
	
	while (!GetAsyncKeyState(VK_RETURN)){}
	

	return StateMainMenu;

}
