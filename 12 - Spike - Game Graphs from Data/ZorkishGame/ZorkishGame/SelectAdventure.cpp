#include "pch.h"
#include <iostream>
#include <string>
#include "SelectAdventure.h"

using namespace std;

States SelectAdventure::runState() {
	displayAdventures();
	return getNextState();
}

void SelectAdventure::displayAdventures() {

	cout << "Zorkish::Select Adventure" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << "Choose your adventure :" << endl;
	cout << endl;
	cout << "	1. Void World" << endl;
	cout << "	2. Water World" << endl;
	cout << "	3. Box World" << endl;
	cout << endl;
	cout << "Select 1-3:> ";

}

States SelectAdventure::getNextState() {

	int opt;
	cin >> opt;

	if (opt == 1) return StateGameplay;
	else if (opt == 2) return StateMainMenu;
	else if (opt == 3) return StateMainMenu;
	else return StateSelectAdventure;

}