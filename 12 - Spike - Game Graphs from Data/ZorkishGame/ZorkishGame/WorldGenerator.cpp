#include "WorldGenerator.h"
#include <cstdlib>
#include <vector>
#include <list>

WorldGenerator::WorldGenerator()
{
	gameObj = GameMap();
}


WorldGenerator::~WorldGenerator()
{
}

GameMap WorldGenerator::initiializeMap(string grid)
{
	myFile.open(grid);
	if (myFile.is_open())
	{
		while (true)
		{
			string str = getAttributes();
			if (str.compare("Fileend") == 0)
				break;
			else if (str.compare("Map") == 0)
			{
				getMap();
			}
		}
	}
	
	return gameObj;
}

string WorldGenerator::getAttributes()
{
	string read = "";
	char c = '\0';

	if (c != '(' && c !='.') {
		
		c = myFile.get();
		while (c != ')') {
			read += c;
			c = myFile.get();
		
		}
	}
	else if (c == '(') {
		c = myFile.peek();
		if (c == '.')	return "Fileend";

	}

	return read;
}

void WorldGenerator::getMap()
{
	string str = "";
	char c = '\0';
	string place = "";

	str = getAttributes();

	if (str.compare("Place") == 0) {
		while (c != '\n') {
			if (c != '\0') {
				place += c;

			}
			c = myFile.get();
		}
	}

	vector<int> mapIDs = vector<int>();
	string mapID = "";
	c = '\0';
	str = getAttributes();
	
	if (str.compare("Connection") == 0) {
		mapID = "";
		while (c != '\n') {
			mapID += myFile.get();
			mapID += myFile.get();

			mapIDs.push_back(stoi(mapID));
			c = myFile.get();
		}
	}

	vector<string> mapDirections = vector<string>();
	string mapDir = "";
	c = '\0';
	str = getAttributes();

	if (str.compare("Directions") == 0) {
		while (c != '\n') {
			if (c != ',') {
				mapDir += c;
			}
			else {
				mapDirections.push_back(mapDir);
				mapDir = "";

			}
			c = myFile.get();
		}
	}

	list<pair<string, int>> gridDir = list<pair<string, int>>();
	pair <string, int> is = pair<string, int>();

	for (int i = 0; i < mapDirections.size(); i++) {
		
		is = { mapDirections[i], mapIDs[i] };
		gridDir.push_back(is);
	}

	gameObj.AddLocation(place,	gridDir);
}
