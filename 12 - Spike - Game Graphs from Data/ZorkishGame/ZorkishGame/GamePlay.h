#pragma once
#include "State.h"
#include "Player.h"
#include "GameMap.h"
#include "WorldGenerator.h"


class GamePlay : public State
{
public:
	GamePlay();
	virtual States runState();
	void runGame();
	States getNextState();

private:
	Player player;
	GameMap gameObj;
	bool flag;
};

