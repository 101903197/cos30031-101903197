#include "Graph.h"
#include <vector>

using namespace std;

Graph::Graph()
{
	graphData = vector <list<pair<string, int>>>();
}


Graph::~Graph()
{
}

void Graph::addConnection(int conni, pair<string, int> connj)
{
	graphData[conni].push_back(connj);
}

list<pair<string, int>> Graph::getConnections(int conni)
{
	return graphData[conni];
}

void Graph::printGraph()
{
	int i = 0;
	for (auto j : graphData)
	{
		if (!j.empty()) {
			cout << i << ": ";

			for (auto temp : j)
			{
				cout << "(" << temp.first << ", " << temp.second << ") ";
			}

			cout << endl;
		}
		i++;
	}
}
