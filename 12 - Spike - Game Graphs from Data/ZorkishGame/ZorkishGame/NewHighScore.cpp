#include "NewHighScore.h"
#include "pch.h"
#include <iostream>
#include <string>
#include <Windows.h>

using namespace std;

States NewHighScore::runState() {
	displayNewHighScore();
	return getNextState();
}

void NewHighScore::displayNewHighScore() {
	cout << "Zorkish::New High Score" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << "Congratulations!" << endl;
	cout << endl;
	cout << "You have made it to the Zorkish Hall Of Fame" << endl;
	cout << endl;
	cout << "Adventure : [the adventure world played]" << endl;
	cout << "Score : [the players score]" << endl;
	cout << "Moves : [number of moves player made]" << endl;
	cout << endl;
	cout << "Please type your name and press enter:> ";
}

States NewHighScore::getNextState() {
	string name;
	cin >> name;

	return StateViewHallOfFame;
}

