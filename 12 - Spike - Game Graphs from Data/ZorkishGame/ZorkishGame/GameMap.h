#pragma once
#include "graph.h"
#include <iostream>
#include <cstdlib>
#include <string>
#include <list>
#include <vector>


using namespace std;

class GameMap
{
public:
	GameMap();
	~GameMap();

	void AddLocation(string place, list <pair <string, int>> gridDir);
	void displayValidDir(int pLoc);
	int isMoveValid(int pLoc, string usermove);
	void displayLocation(int pLoc);
	void displayMap();

private:	
	Graph gridMap;
	vector<string> mapLocs;
	
};

