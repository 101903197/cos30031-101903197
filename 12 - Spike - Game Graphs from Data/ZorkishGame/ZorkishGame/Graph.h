#pragma once
#include <iostream>
#include <string>
#include <list>
#include <vector>

using namespace std;

class Graph
{
public:
	Graph();
	~Graph();

	void addConnection(int conni , pair<string, int> connj );

	list<pair<string, int>> getConnections(int conni);

	void printGraph();

private:
	vector<list<pair<string, int>>> graphData;

};

