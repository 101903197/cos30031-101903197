#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include "GameMap.h"

using namespace std;

class WorldGenerator
{
public:
	WorldGenerator();
	~WorldGenerator();

	GameMap initiializeMap(string grid);

private:
	ifstream myFile;
	
	string getAttributes();
	void getMap();

	GameMap gameObj;
	
};

