#include "Player.h"
#include <iostream>
#include <string>

Player::Player()
{
	string bagpack[20] = {"","", "", "", "", "", "", "", "", "", "", "", "","","", "", "", "", "", ""};
	tLoc = 0;
}


void Player::addItem(string item) {
	int i = 0;
	while (!bagpack[i].empty()) {
		i++;
	}

	if (i == 20) {
		cout << "Bag is full. Please drop some item to pick up this item." << endl;
	}
	else{
		bagpack[i] = item;
		cout << item + " is added to bagpack." << endl;
	}

	
}

void Player::openBag(){
	int count = 0;
	while (!bagpack[count].empty()) {
		count++;
	}
	if(count == 0 ) {
		cout << "Bag is empty" << endl;
	}
	else {
		for (int i = 0; i <= count;i++) {
			if (!bagpack[i].empty()) {
				cout << bagpack[i] << endl;
			}
		}
	}
}

void Player::dropItem(string item){
	
	int count = 0;
	while (!bagpack[count].empty()) {
		count++;
	}
	
	for (int i = 0; i <= count; i++)
	{
		if (bagpack[i] == item) {

			for (int j = i; j <= count-1; j++) {
				cout << item + " is dropped from bagpack." << endl;
				bagpack[j] = bagpack[j + 1];
			}
			break;
		}
	}
}

int Player::getLocation()
{
	return tLoc;
}

void Player::setLocation(int pLoc)
{
	tLoc = pLoc;
}
