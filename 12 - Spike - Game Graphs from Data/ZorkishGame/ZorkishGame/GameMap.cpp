#include "GameMap.h"
#include "Graph.h"
#include <string>
#include <cstdlib>
#include <vector>
#include <list>
#include <stdio.h>
#include <locale>

using namespace std;

GameMap::GameMap()
{
	gridMap = Graph();
	 mapLocs = vector<string>();
}


GameMap::~GameMap()
{
}

void GameMap::AddLocation(string place, list<pair<string, int>> gridDir)
{
	mapLocs.push_back(place);

	for (auto i : gridDir)
	{
		gridMap.addConnection(mapLocs.size() - 1, i);
	}

}

void GameMap::displayValidDir(int pLoc)
{
	
	
	for (auto i : gridMap.getConnections(pLoc)) {
		cout << i.first << endl;
	}

}

int GameMap::isMoveValid(int pLoc,  string usermove)
{
	locale loc;
	for (auto i : gridMap.getConnections(pLoc)) {
		string str = i.first;
		if (usermove.find(str) != string::npos)
			return i.second;
	}


	return pLoc;
}

void GameMap::displayLocation(int pLoc)
{
	string currLoc;
	try {
		currLoc = mapLocs.at(pLoc);
		cout << "Adventurer, you are currently at: " << currLoc << ". Places you could go are:" << endl;
		displayValidDir(pLoc);
		
	}
	catch (exception e) {
		cout << "There is no way over that side!" << endl;
	}
}

void GameMap::displayMap()
{
	gridMap.printGraph();

}

