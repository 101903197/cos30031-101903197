// gridworldpt1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#define R 10
#define C 10

using namespace std;

class gridworldpt1 {
public:
	int grid[R][C] = {  {1,1,1,1,1,1,1,1,1,1},
						{1,0,0,0,0,0,0,0,2,1},
					    {1,2,1,1,1,0,0,0,0,1},
						{1,0,0,0,1,3,0,0,0,1},
						{1,0,1,0,1,1,1,0,1,1},
						{1,0,1,0,1,0,0,0,0,1},
						{1,0,1,0,1,0,0,0,0,1},
						{1,0,0,0,0,0,1,0,0,1},
						{1,0,0,0,0,0,0,0,0,1},
						{1,1,1,1,1,1,1,1,1,1} };
	int pi = 8;
	int pj = 3;
	string pstatus = "alive";

	void playtime() {
		
		while (pstatus == "alive") {
			cout << "You can move";

			
			if (grid[pi-1][pj] >= 0 && grid[pi - 1][pj] != 1) {
				cout << "   n (North)   ";
			}
						
			if (grid[pi+1][pj] >= 0 && grid[pi + 1][pj] != 1) {
				cout << "   s (South)   ";
			}
						
			if (grid[pi][pj-1] >= 0 && grid[pi][pj - 1] !=1) {
				cout << "   w (West)   ";
			}
			
			if (grid[pi][pj+1] >= 0 && grid[pi][pj + 1] !=1) {
				cout << "   e (East)   ";
			}
			
			cout << endl;

			char move='o';
			cout << "Please enter your move : ";
			cin >> move;
			cout << endl;

			if (move == 'n' && grid[pi - 1][pj] != 1) {
				cout << "You moved North ! "<< endl;
				pi -= 1;
			}
			else if (move == 's' && grid[pi + 1][pj] != 1) {
				cout << "You moved South ! " << endl;
				pi += 1;
			}
			else if (move == 'w' && grid[pi][pj - 1] != 1) {
				cout << "You moved West ! " << endl;
				pj -= 1;
			}
			else if (move == 'e' && grid[pi][pj + 1] != 1) {
				cout << "You moved East ! " << endl;
				pj += 1;
			}
			else if (move == 'q') {
				exit(0);
			}
			else {
				cout << "Wrong command ! No moves ! " << endl;

			}

			if (grid[pi][pj] == 3)
			{
				cout << "You have successfully conquered this game and won it ! I wish you all the luck spending all those gold and gems !" << endl;
				pstatus = "won";
				exit(0);
			}
			else if (grid[pi][pj] == 2)
			{
				cout << "Ahhhhhhhhhhhhhhh! That was the pit i was talking about ! Sorry You are dead !" << endl;
				pstatus = "dead";
				exit(0);
			}
			else {
				pstatus = "alive";
			}
		}
	}


};

int main()
{
	gridworldpt1 player1;

    cout << "Welcome to the World of grid ! You will need four commands to win this game (n,s,w,e - referring to the four directs) \n, that is if you don't die by falling into the deadly pit. If you survive \n you will find a chest with loads of gems and gold ! Good luck Wanderer >>>"; 
	cout << endl;

	player1.playtime();
	return 0;
}
