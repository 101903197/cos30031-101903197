// imagetoggle.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <conio.h>
#include <string>
#include <SDL_mixer.h>
#include "SDL.h"
#include <SDL_image.h>
#undef main

using namespace std;

int main()
{
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_Surface *winSurface = nullptr;
	
	SDL_Surface *background = nullptr;
	SDL_Surface *foreground = nullptr;
	SDL_Surface *bimg = nullptr;
	SDL_Surface *currentImage = nullptr;
	SDL_Surface *currentFImage = nullptr;
	SDL_Surface *currentBImage = nullptr;


	SDL_Window *window = SDL_CreateWindow("Image Spike", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_OPENGL);
	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	SDL_Renderer *renderer1 = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	background = IMG_Load("image1.jpg");
	bimg = IMG_Load("whiteback.jpg");
	currentBImage = background;
	currentImage = background;


	foreground = IMG_Load("smallimg.png");
	SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, foreground);
	

	int width = 0;
	int height = 0;

	//SDL_QueryTexture(foreground, NULL, NULL, &width, &height);

	
	const int clipPerColumn = 3;
	//SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, foreground);
	
	SDL_Texture* clip[clipPerColumn];
	/*
	for (int j = 0; j < clipPerColumn; j++)
	{
		clip[j] = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, width , height / clipPerColumn);
		SDL_SetTextureBlendMode(clip[j], SDL_BLENDMODE_BLEND);
		SDL_Rect rect = { 0 ,j*height / clipPerColumn, width , height / clipPerColumn };
		SDL_SetRenderTarget(renderer, clip[j]);
		SDL_RenderCopy(renderer, foreground, &rect, NULL);
		//SDL_RenderPresent(renderer);
	}
	SDL_SetRenderTarget(renderer, NULL);*/
	winSurface = SDL_GetWindowSurface(window);


	SDL_SetRenderDrawColor(renderer, 200, 234, 123, 50);

	bool running = true;
	SDL_Event event;

	cout << "Welcome. This is a very basic music player." << endl;
	cout << "It has three songs which can be accessed by number 1,2 and 3. " << endl;
	cout << "To Play/Pause the songs down key is pressed, 0 is used to toggle between songs." << endl;

	bool bflag = true;
	bool fflag = true;
	while (running) {

		while (SDL_PollEvent(&event) != 0) {
			if (event.type == SDL_QUIT)
			{
				running = false;
			}
			else if (event.type == SDL_KEYDOWN) {

				if (event.key.keysym.sym == SDLK_0) {
					if (bflag) {
						bflag = false;
						currentBImage = bimg;

					}
					else {
						bflag = true;
						currentBImage = background;

					}
					currentImage = currentBImage;
				}
				else if (event.key.keysym.sym == SDLK_1) {
					cout << "..........." << endl;
					if (true) {
						//fflag = false;
						//currentFImage = currentBImage;

						//SDL_Rect rect = { 10, 10, width, height / clipPerColumn };
						//SDL_RenderCopy(renderer, clip[1], NULL, &rect);
						//SDL_RenderPresent(renderer);
						SDL_RenderClear(renderer);
						SDL_RenderCopy(renderer, texture , NULL, NULL);
						SDL_RenderPresent(renderer);
					}
					else {
						//fflag = true;
						//SDL_RenderClear(renderer);

					}
					//currentImage = currentFImage;
				}
				/*else if (event.key.keysym.sym == SDLK_2) {
					if (fflag) {
						//fflag = false;
						//currentFImage = currentBImage;

						SDL_Rect rect = { 10, 10 + ((1 * height) / clipPerColumn), width, height / clipPerColumn };
						SDL_RenderCopy(renderer, clip[2], NULL, &rect);
						SDL_RenderPresent(renderer);
					}
					else {
						//fflag = true;
						//currentFImage = foreground;
						SDL_RenderClear(renderer);

					}
					//currentImage = currentFImage;
				}
				else if (event.key.keysym.sym == SDLK_3) {
					if (fflag) {
						//fflag = false;
						currentFImage = currentBImage;
						SDL_Rect rect = { 10, 10 + ((2 * height) / clipPerColumn), width, height / clipPerColumn };
						SDL_RenderCopy(renderer, clip[2], NULL, &rect);
						SDL_RenderPresent(renderer);

					}
					else {
						//fflag = true;
						//currentFImage = foreground;
						SDL_RenderClear(renderer);
					}
					//currentImage = currentFImage;
				}*/
				
				
				//SDL_BlitSurface(currentImage,NULL,winSurface,NULL);

				SDL_UpdateWindowSurface(window);

			}


		}


	}
	SDL_FreeSurface(background);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	
	bimg = background = nullptr;
	foreground = nullptr;
	window = nullptr;
	SDL_Quit();

	return 0;

}
