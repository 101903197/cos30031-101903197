// imagetoggle1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include "SDL.h"
#include <SDL_image.h>
#undef main

using namespace std;

SDL_Window *window = nullptr;
SDL_Renderer *renderer = nullptr;

bool f1flag;
bool f2flag;
bool f3flag;

const int clipPerColumn = 3;

SDL_Texture *bgtext = nullptr;
SDL_Event event;
SDL_Surface *winSurface = nullptr;
SDL_Texture *fgtext = nullptr;
SDL_Texture* clip[clipPerColumn];

bool isRunning;
bool bflag;

int width;
int height;


SDL_Texture* loadTexture(string path)
{
	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path


	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", IMG_GetError());
	}
	else
	{
		//Create texture from surface pixels
		newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
		if (newTexture == NULL)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", SDL_GetError());
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	return newTexture;
}



int main()
{
	SDL_Init(SDL_INIT_EVERYTHING);


	
	

	window = SDL_CreateWindow("Image Spike", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_OPENGL);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	bgtext=loadTexture("tile.png");
	fgtext = loadTexture("water.png");

	SDL_QueryTexture(fgtext, NULL, NULL, &width, &height);

	SDL_SetRenderDrawColor(renderer, 255,255,255,255);
	winSurface = SDL_GetWindowSurface(window);

	for (int j = 0; j < clipPerColumn; j++)
	{
		clip[j] = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, width, height / clipPerColumn);
		SDL_SetTextureBlendMode(clip[j], SDL_BLENDMODE_BLEND);
		SDL_Rect rect = { 0 ,j*height / clipPerColumn, width , height / clipPerColumn };
		SDL_SetRenderTarget(renderer, clip[j]);
		SDL_RenderCopy(renderer, fgtext, &rect, NULL);
		

	}
	SDL_SetRenderTarget(renderer, NULL); 


	isRunning = true;
	bflag = false;
	f1flag = false;
	f2flag = false;
	f3flag = false;
	while (isRunning) {
		while (SDL_PollEvent(&event) != 0) {
			if (event.type == SDL_QUIT)
			{
				isRunning = false;
			}
			else if (event.type == SDL_KEYDOWN) {
				cout << "1" << endl;
				if (event.key.keysym.sym == SDLK_0) {
					cout << "2" << endl;
					if (!bflag) {
						cout << "3" << endl;
						
						//Clear screen
						SDL_RenderClear(renderer);

						//Render texture to screen
						SDL_RenderCopy(renderer, bgtext, NULL, NULL);

						//Update screen
						SDL_RenderPresent(renderer);


						bflag = true;
					}
					else {
						cout << "4" << endl;
						
						//Clear screen
						SDL_RenderClear(renderer);
						SDL_RenderPresent(renderer);

						bflag = false;
					}
				}
				else if (event.key.keysym.sym == SDLK_1) {
					
					if (!f1flag) {
						cout << "3" << endl;

						//Clear screen
						SDL_RenderClear(renderer);

						SDL_Rect rect = { 0 ,0, width , height / clipPerColumn };

						//Render texture to screen
						if (bflag==true) {
							SDL_RenderCopy(renderer, bgtext, NULL, NULL);
							SDL_RenderCopy(renderer, clip[0], NULL, &rect);

							


						}
						else
						{
							SDL_RenderCopy(renderer, clip[0], NULL, &rect);

						}
						//Update screen
						SDL_RenderPresent(renderer);


						f1flag = true;
					}
					else {
						cout << "4" << endl;

						//Clear screen
						SDL_RenderClear(renderer);
						
						if (bflag==true) {
							SDL_RenderCopy(renderer, bgtext, NULL, NULL);
						}

						SDL_RenderPresent(renderer);

						f1flag = false;
					}
				}
				else if (event.key.keysym.sym == SDLK_2) {
					
					if (!f2flag) {
						cout << "3" << endl;

						//Clear screen
						SDL_RenderClear(renderer);

						SDL_Rect rect = { 0 ,1*height / clipPerColumn, width , height / clipPerColumn };

						//Render texture to screen
						if (bflag == true) {
							SDL_RenderCopy(renderer, bgtext, NULL, NULL);
							SDL_RenderCopy(renderer, clip[1], NULL, &rect);

						}
						else
						{
							SDL_RenderCopy(renderer, clip[1], NULL, &rect);

						}

						//Update screen
						SDL_RenderPresent(renderer);


						f2flag = true;
					}
					else {
						cout << "4" << endl;

						//Clear screen
						SDL_RenderClear(renderer);

						if (bflag == true) {
							SDL_RenderCopy(renderer, bgtext, NULL, NULL);
						}

						SDL_RenderPresent(renderer);

						f2flag = false;
					}
				}
				else if (event.key.keysym.sym == SDLK_3) {
					
					if (!f3flag) {
						cout << "3" << endl;

						//Clear screen
						SDL_RenderClear(renderer);

						SDL_Rect rect = { 0 ,2 * height / clipPerColumn, width , height / clipPerColumn };

						//Render texture to screen
						if (bflag == true) {
							SDL_RenderCopy(renderer, bgtext, NULL, NULL);
							SDL_RenderCopy(renderer, clip[2], NULL, &rect);


						}
						else
						{
							SDL_RenderCopy(renderer, clip[2], NULL, &rect);

						}
						//Update screen
						SDL_RenderPresent(renderer);


						f3flag = true;
					}
					else {
						cout << "4" << endl;

						//Clear screen
						SDL_RenderClear(renderer);

						if (bflag == true) {
							SDL_RenderCopy(renderer, bgtext, NULL, NULL);
						}

						SDL_RenderPresent(renderer);

						f3flag = false;
					}
				}
				
			}
			
		
		}
	}

	//Free loaded image
	SDL_DestroyTexture(bgtext);
	bgtext = NULL;

	//Destroy window    
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	window = NULL;
	renderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();

	return 0;

}
