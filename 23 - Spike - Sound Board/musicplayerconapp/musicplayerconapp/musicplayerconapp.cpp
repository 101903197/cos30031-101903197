// musicplayerconapp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <conio.h>
#include <string>
#include <SDL_mixer.h>
#include "SDL.h"
#undef main


using namespace std;

int main()
{
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_Surface *winSurface = nullptr;
	SDL_Window *window = SDL_CreateWindow("Music Player",SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_OPENGL);
	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	winSurface = SDL_GetWindowSurface(window);

	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);

	Mix_Music *music1 = Mix_LoadMUS("DesiJourney.wav");
	Mix_Music *music2 = Mix_LoadMUS("DiscordToday.wav");
	Mix_Music *music3 = Mix_LoadMUS("MoodyLoop.wav");
	Mix_Music *currentmusic = nullptr;

	currentmusic = nullptr;

	SDL_SetRenderDrawColor(renderer, 200,234,123,50);

	bool running = true;
	SDL_Event event;

	cout << "Welcome. This is a very basic music player." << endl;
	cout << "It has three songs which can be accessed by number 1,2 and 3. " << endl;
	cout << "To Play/Pause the songs down key is pressed, 0 is used to toggle between songs." << endl;
	cout << "000" << endl;

	bool flag = true;
	while (running) {
		
		while (SDL_PollEvent(&event)!=0) {
			if (event.type == SDL_QUIT)
			{
				
				running = false;
			}
			else if (event.type == SDL_KEYDOWN) {				
				
				if (event.key.keysym.sym == SDLK_1) {
					currentmusic = music1;
					flag = true;
					cout << "Music 1" << endl;
					Mix_PlayMusic(currentmusic, -1);

				}
				else if (event.key.keysym.sym == SDLK_2) {
					currentmusic = music2;
					flag = true;
					cout << "Music 2" << endl;
					Mix_PlayMusic(currentmusic, -1);

				}
				else if (event.key.keysym.sym == SDLK_3) {
					currentmusic = music3;
					flag = true;
					cout << "Music 3" << endl;
					Mix_PlayMusic(currentmusic, -1);

				}
				else if (event.key.keysym.sym == SDLK_0) {
					if (flag) {
						Mix_PauseMusic();
						cout << "Music Paused" << endl;
						flag = false;
					}
					else {
						Mix_ResumeMusic();
						cout << "Music  Resumed" << endl;
						flag = true;
					}
					

				}			
				SDL_UpdateWindowSurface(window);
				
			}
		

		}
		

	}
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	Mix_FreeMusic(music1);
	Mix_FreeMusic(music2);
	Mix_FreeMusic(music3);
	Mix_CloseAudio();
	SDL_Quit();
		
	return 0;

}
