// passing parameters by reference
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include<string>

using namespace std;

class basicc {
private:
	int privatenum=20;
public:
	int publicnum=10;
	int getprivatenum() { return privatenum; }
};

void display(int a, int b){
	cout << "Values are " << a << " and " << b << endl;
}

int multi(int a) {
	cout << "Multiplying the value by 3 ! "<< endl;
	return a * 3;
}

void printarray() {
	int arr[5];
	for (int i = 0; i < 5; i++)
	{
		arr[i] = rand() % 100;
	}
	cout << "Array is " << endl;

	for (int i = 0; i < 5; i++)
	{
		cout << arr[i] << endl;
	}

}

void oddloop() {
	for (int i = 0; i < 20; i++)
	{
		if (i % 2 != 0)
		{
			cout <<  i << " " ;
		}
	}
	cout << endl;
}

void splitline(string str) {
	for (int i = 0; i < str.length(); i++)
	{
		cout << str.at(i);

		if (str.at(i) == ' ')
		{
			cout << endl;
		}
	}
}

int main()
{
	basicc obj;
	int x = 45, y = 54;
	display(x, y);
	int mans=multi(25);
	cout << "Answer of multiply is " << mans << endl;

	int value = 25;
	int *valuepointer = &value;
	cout << "Value : " << value << endl;
	cout << "Value Pointer : " << *valuepointer << endl;

	*valuepointer = 30;
	cout << "Value : " << value << endl;
	cout << "Value Pointer : " << *valuepointer << endl;

	oddloop();

	printarray();
	splitline("this has spaces in it");

	cout << endl;
	cout << "Public int of class value : " << obj.publicnum << endl;
	cout << "Private int of class value : " << obj.getprivatenum() << endl;


	return 0;
}